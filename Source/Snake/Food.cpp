// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Components/StaticMeshComponent.h"

AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AFood::BeginPlay()
{
	Super::BeginPlay();
	Type = static_cast<EFoodType>(FMath::RandRange(0, static_cast<int>(EFoodType::Length) - 1));
	auto meshActor = GetComponentByClass(UStaticMeshComponent::StaticClass());
	if (meshActor)
	{
		auto staticMesh = Cast<UStaticMeshComponent>(meshActor);
		staticMesh->SetMaterial(0, MaterialsByType[Type]);
	}
}

void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool IsHead)
{
	if (!IsHead || !IsValid(Interactor))
		return;
	auto snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(snake))
	{
		snake->AddSnakeElement();
		if (Type == EFoodType::SpeedUp)
		{
			snake->SpeedUp();
		}
		else if (Type == EFoodType::SpeedDown)
		{
			snake->SpeedDown();
		}
		Destroy();
	}
}

