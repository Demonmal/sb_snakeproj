// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableItem.h"
#include "Food.generated.h"

UENUM()
enum class EFoodType
{
	SpeedUp,
	SpeedDown,
	Length
};

UCLASS()
class SNAKE_API AFood : public AActor, public IInteractableItem
{
	GENERATED_BODY()
	
public:	
	AFood();

	UPROPERTY()
	EFoodType Type;

	UPROPERTY(EditDefaultsOnly)
	TMap<EFoodType, UMaterialInterface*> MaterialsByType;

protected:
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void Interact(AActor* Interactor, bool IsHead) override;
};
