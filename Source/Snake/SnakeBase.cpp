// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "InteractableItem.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100;
	TickInterval = 10;
	TickIntervalDelta = 1;
	LastMoveDirectionType = EMoveDirectionType::UP;
}

void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(TickInterval);
	AddSnakeElement(4);
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	if (ElementsNum < 1)
		return;
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector newElementOffset = FVector(ElementSize * SnakeElements.Num(), 0, 0);
		FTransform newElementTransform = FTransform(GetActorLocation() - newElementOffset);
		ASnakeElementBase* newElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, newElementTransform);
		newElement->SetOwner(this);
		int32 elementIndex = SnakeElements.Add(newElement);
		if (elementIndex == 0)
		{
			newElement->SetFirstElement();
		}
		else
		{
			SetElementToEnd(newElement, elementIndex);
		}
	}
}

void ASnakeBase::SetElementToEnd(ASnakeElementBase* SnakeElement, int ElementIndex)
{
	if (ElementIndex == 0)
		return;
	auto prevElement = SnakeElements[ElementIndex - 1];
	FVector newElementPoint = prevElement->GetActorLocation();
	switch (LastMoveDirectionType)
	{
	case EMoveDirectionType::UP:
		newElementPoint.X -= ElementSize;
		break;
	case EMoveDirectionType::DOWN:
		newElementPoint.X += ElementSize;
		break;
	case EMoveDirectionType::RIGHT:
		newElementPoint.Y -= ElementSize;
		break;
	case EMoveDirectionType::LEFT:
		newElementPoint.Y += ElementSize;
		break;
	}
	SnakeElement->SetActorLocation(newElementPoint);
}

void ASnakeBase::Move()
{
	FVector movementVector(ForceInitToZero);

	switch (LastMoveDirectionType)
	{
	case EMoveDirectionType::UP:
		movementVector.X += ElementSize;
		break;
	case EMoveDirectionType::DOWN:
		movementVector.X -= ElementSize;
		break;
	case EMoveDirectionType::RIGHT:
		movementVector.Y += ElementSize;
		break;
	case EMoveDirectionType::LEFT:
		movementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto curElement = SnakeElements[i];
		auto prevElement = SnakeElements[i - 1];
		FVector prevPoint = prevElement->GetActorLocation();
		curElement->SetActorLocation(prevPoint);
	}

	SnakeElements[0]->AddActorWorldOffset(movementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::TrySetNewMoveDirection(EMoveDirectionType directionType)
{
	bool isUnacceptableVertDirection = (directionType == EMoveDirectionType::DOWN && LastMoveDirectionType == EMoveDirectionType::UP) 
		|| (directionType == EMoveDirectionType::UP && LastMoveDirectionType == EMoveDirectionType::DOWN);
	bool isUnacceptableHorizontalDirection = (directionType == EMoveDirectionType::LEFT && LastMoveDirectionType == EMoveDirectionType::RIGHT)
		|| (directionType == EMoveDirectionType::RIGHT && LastMoveDirectionType == EMoveDirectionType::LEFT);
	if (!isUnacceptableVertDirection && !isUnacceptableHorizontalDirection)
	{
		LastMoveDirectionType = directionType;
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* SnakeElement, AActor* Other)
{
	if (IsValid(SnakeElement))
	{
		int32 elementIndex = SnakeElements.Find(SnakeElement);
		bool isFirst = elementIndex == 0;
		auto interectibleItem = Cast<IInteractableItem>(Other);
		if (interectibleItem)
		{
			interectibleItem->Interact(this, isFirst);
		}
	}
}

void ASnakeBase::SpeedUp()
{
	TickInterval -= TickIntervalDelta;
}

void ASnakeBase::SpeedDown()
{
	TickInterval += TickIntervalDelta;
}
