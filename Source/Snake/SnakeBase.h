#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMoveDirectionType
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float TickInterval;

	UPROPERTY(EditDefaultsOnly)
	float TickIntervalDelta;

private:
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMoveDirectionType LastMoveDirectionType;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);

	void Move();

	void TrySetNewMoveDirection(EMoveDirectionType directionType);

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* SnakeElement, AActor* Other);

	UFUNCTION()
	void SpeedUp();

	UFUNCTION()
	void SpeedDown();

private:
	void SetElementToEnd(ASnakeElementBase* SnakeElement, int ElementIndex);
};
