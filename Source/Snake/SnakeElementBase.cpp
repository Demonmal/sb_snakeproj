// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include <Runtime\Engine\Classes\Kismet\KismetSystemLibrary.h>

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Mesh->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetFirstElement_Implementation()
{
	Mesh->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::Interact(AActor* Interactor, bool IsHead)
{
	if (!IsValid(Interactor))
		return;
	auto snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(snake))
	{
		auto thisWorld = GetWorld();
		UKismetSystemLibrary::QuitGame(thisWorld, thisWorld->GetFirstPlayerController(), TEnumAsByte<EQuitPreference::Type>::EnumType::Quit, false);
	}
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementBase::ToggleCollision()
{
	Mesh->SetCollisionEnabled(Mesh->IsCollisionEnabled() ? ECollisionEnabled::NoCollision : ECollisionEnabled::QueryOnly);
}

void ASnakeElementBase::SetOwner(ASnakeBase* Owner)
{
	SnakeOwner = Owner;
}